package com.nostis.library;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Runnable {

	public static void main(String[] args) {
		SpringApplication.run(Runnable.class, args);
	}

}
