package com.nostis.library.dao;

import com.nostis.library.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookCrud extends JpaRepository<Book, Long> {
}
