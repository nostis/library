package com.nostis.library.dao;

import com.nostis.library.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleCrud extends JpaRepository<Role, Long> {
}
