package com.nostis.library.model;

import lombok.Data;

import java.util.Date;
import java.util.Optional;

@Data
public class UserHistory {
    private Long userId;
    private Date dateLent;
    private Date dateReturned;

    public Optional<Date> getDateReturned(){
        return Optional.ofNullable(dateReturned);
    }
}