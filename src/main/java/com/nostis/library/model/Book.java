package com.nostis.library.model;

import lombok.Data;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class Book {
    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String isbn;
    private String publisher;
    private String language;
    private Date publishedDate;
    private Status status;
    @ElementCollection
    private List<Author> authors;
    @ElementCollection
    private List<UserHistory> userHistory;
}
