package com.nostis.library.model;

public enum Status {
    LENT,
    AVAILABLE,
    UNAVAILABLE
}
